const express = require('express');
const app = express();
const db = require("./db");

//Conectar a la db de mongo
db.connect();

//Habilitar express.json
app.use(express.json({extended:true}));

//puerto de la app
const PORT =process.env.PORT || 4000;


//Importar rutas
app.use('/api/comentarios', require('./routes/comentarios'));

//Para los errores
app.use('*', (req, res, next)=> {
    const error = new Error('Route not found');
    error.status=404;
    next(error);
});
app.use((err, req, res, next)=> {
    return res.status(err.status || 500).json('error',{
        message:err.message,
        status:err.status
    });
})

//arrancar la app
app.listen(PORT, () => {
    console.log(`EL servidor está inicializado en el puerto ${PORT} `)
});