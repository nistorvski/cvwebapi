const mongoose = require('mongoose');

const ComentarioSchema = mongoose.Schema({
    nombre: {
        type:String,
        required:true,
        trim:true
    },
    mensaje: {
        type:String,
        required:true,
    },
    imagen:{
        type:String
    }

});
module.exports = mongoose.model('Comentario', ComentarioSchema);