const Comentario = require('../models/Comentario');

exports.crearComentario = async (req, res) => {
    try{
 
        //Crear un comentario
        const comentario =  await new Comentario(req.body);
        comentario.save();
        res.json(comentario);

    }catch(error){
        console.log(error);
        res.status(500).send('Hubo un error');
    }
}
//Obtener los comentarios

exports.getComentarios = async (req, res)=> {
    try{
        const comentarios = await Comentario.find();
        res.json(comentarios);

    }catch(error){
        console.log(error);
        res.status(500).send('Error getComentarios');
    }
}

//Actualizar/editar comentario

// exports.putComentario = async(req, res) => {

// }