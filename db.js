const mongoose = require('mongoose');
require('dotenv').config({ path: 'variables.env' });

const DB_URL = "mongodb://localhost:27017/cvweb_api";

const connect = async () => {
   try{
        await mongoose.connect(process.env.DB_MONGO, {
            useNewUrlParser:true,
            useUnifiedTopology:true,
            useFindAndModify:false
        });
        console.log('DB de Mongo Conectada con exito');
   }catch(error) {
       console.log(error);
       process.exit(1);//Esto detiene la app si pas alago.
   }
}

module.exports={DB_URL, connect};