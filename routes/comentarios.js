const express = require('express');
const router = express.Router();
const comentarioController= require('../controllers/comentarioController');

router.post('/',
comentarioController.crearComentario
);
router.get('/',
comentarioController.getComentarios
);


module.exports = router;